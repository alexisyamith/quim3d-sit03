{
    
    "reactor" : {



    		"comoJugar":"To successfully complete this lab, complete the following procedures.\r\n\r\n
1. Identify the lab materials and functions.\r\n
\t\t\▪ Locate the synthesis reactor with actuators and automatic sensors.\r\n
\t\t\▪ Locate the control switches to adjust the following parameters:\r\n
\t\t\t\t\ ‣‣ Reagent solutions (A, B, C, and D): click to switch to \r\n\t\t\t\t\ add solution in liters (L).\r\n
\t\t\t\t\ ‣‣ Solvent: click to add solution in liters(L).\r\n
\t\t\t\t\ ‣‣ Temperature:  click the "+/-" button to regulate the temperature (ºC).\r\n
\t\t\t\t\ ‣‣ Agitator: click the "Shake on/off" button, the "+/-" button to adjust the rpm \r\n\t\t\t\t\(revolutions per minute).\r\n
\t\t\t\t\ ‣‣ Amino acid process: click to condition the reagents after \r\n\t\t\t\t\ combining with the solvent.\r\n
\t\t\t\t\ ‣‣ Verification process: click to allow the amino acid to be \r\n\t\t\t\t\ processed after conditioning.\r\n
\t\t\t\t\ ‣‣ Final Test: click to verify the correct amino acid synthesis with an \r\n\t\t\t\t\ automated colorimetric test.\r\n
\t\t\▪ Locate the parameter table and indicator table to monitor the various \r\n\t\t\processes.\r\n
\t\t\▪ The mission (challenge) is to correctly synthesize the amino acid using \r\n\t\t\ the parameters given and to verify the result using a colorimetric test. \r\n\t\t\ The end result should be red.\r\n
\t\t\▪ The lab may be reset by clicking on the red "Reset" button. This will \r\n\t\t\ register an attempt.\r\n
\t\t\▪ A "Screen" button is also available to view a temperature and \r\n\t\t\ time graph, equations for the process, and the final data.\r\n\r\n

2. Prepare the reagent A. \r\n
\t\t\▪ Add solution A to the conditioning chamber. Solution A should \r\n\t\t\ comprise
60% of the total 50 mL needed for synthesis.\r\n
\t\t\▪ Add an equal amount of the solvent to the conditioning chamber.\r\n
\t\t\▪ Agitate the solution at 100 rpm.\r\n
\t\t\▪ Then move the solution to the process chamber (click the amino \r\n\t\t\ process).\r\n\r\n

3. Prepare the reagent C. \r\n
\t\t\▪ Add solution C to the conditioning chamber. Solution C should \r\n\t\t\ comprise
\r\n\t\t\ 40% of the total 50 mL needed for synthesis.\r\n
\t\t\▪ Add an equal amount of the solvent to the conditioning chamber.\r\n
\t\t\▪ Agitate the solution at 100 rpm.\r\n
\t\t\▪ Then move the solution to the process chamber, click "Amino" process.\r\n\r\n

4. Combine the reagents for synthesis. \r\n
\t\t\▪ Increase the temperature to 40º C.\r\n
\t\t\▪ Increase the agitation to 150 rpm.\r\n
\t\t\▪ After a short time, turn agitation off.\r\n\r\n

5. Verify the synthesis of the amino acid (glycine). \r\n
\t\t\▪ Move the mixed reagents to the verification chamber, click "Verification"
\r\n\t\t\ process.\r\n
\t\t\▪ To add the substance for the colorimetric test, click "Final test".\r\n
\t\t\▪ If the final results are red then the correct amino acid and process 
\r\n\t\t\ was obtained.\r\n\r\n

Completion of lab, answer the evaluation questions, and generate the lab report. \r\n
\t\t\▪ If the correct result was obtained, a success message will appear.
\r\n\t\t\Click "Continue".\r\n
\t\t\▪ Add an equal amount of the solvent to the conditioning chamber.\r\n\r\n

Optional: FREE PRACTICE \r\n
This lab has a free practice mode to allow for opening, ending, discovery, and experimentation. After navigating to the lab station, choose the free practice, using the arrow button on the title screen.\r\n",

"misionJuego" : "Welcome to the virtual chemistry lab for studying the synthesis of amino acids. For your challenge, you will find elements and equipment on the lab table for the synthesis of amino acids. This includes an area with reagents and a solvent for the synthesis of amino acids, a reagents conditioning chamber, and the process chamber. There is also a test chamber, in which you can test or verify the quality of the amino acids.  You must prepare fifty (50) milliliters of an amino acid (glycine) with 60% of A and 40% of C. Note that at the end of the challenge, the result will be checked using a calorimetric test.",

  "Instructions": "To find the reactor data and calculations associated with the synthesis of amino acids click the “Equations” button.
    
 Click the “Calculator” button to perform the mathematical operations required for solving the equations and completing the challenge. ",

  		
	  
   	 	"preguntaOrientadora1" : " Why are amino acids important in the human body? Please explain.",
   	 	"preguntaOrientadora2" : " Are amino acids formed by a peptide bond? Please explain.",
   	 	"preguntaOrientadora3" : " What are the main essential amino acids? Please explain. ",
   	 	"preguntaOrientadora4" : " Are there amino acids with functions in medicine? Please explain.",
   	 	"preguntaOrientadora5" : " Some amino acids are nonessential. Give examples of them and their application in the human diet.",
		
		
   	 	"preguntaEvaluacion1" : " In the preparation of the reagents, a solvent is used and shaked at 100 rpm.:true",
   	 	"preguntaEvaluacion2" : " The temperature required to produce a reaction in the chamber is 60ºC.:false",
   	 	"preguntaEvaluacion3" : " The solvent used must be mixed with the reagents in the same proportion.:true",
   	 	"preguntaEvaluacion4" : " To obtain very good results, the pH must be 10, as it is an amino acid with basic characteristics.:false",
   	 	"preguntaEvaluacion5" : " The percentage of solution C is 40% in volume.:true",


   	 	"comoJugarPracticaLibre" :     "	To perform calculations, use the data provided in the virtual lab.\r\n
▪	Click the ”Screen” button to see the equations associated with the synthesis of amino acids.\r\n
▪	To synthesize amino acids, perform the following steps:\r\n
‣‣	Check that the solutions (reagents) are ready to use (a message indicates that the solutions are now available).\r\n 
‣‣	Verify that solvent solutions are ready to use.\r\n
‣‣	Add the reagents to the reagent conditioning chamber considering the following:\r\n
	- The suggested solution 1 to be combined can be (A,B, C, D) (60%) percentage.\r\n
	- The suggested solution 2 to be combined can be (A,B, C, D) (40%) percentage.\r\n
	- Add the solvent in the same amount.\r\n
	- Shake the solution at 100 rpm.\r\n
	- Perform each process for each reagent independently in the chamber.\r\n
‣‣	When reagents are in the process chamber, adjust the temperature to 40 ° C (Celsius degrees) and place the shaker at 150 rpm.\r\n
‣‣	Remove the sample from the process chamber and send it to the test chamber.\r\n
‣‣	Add and test fluid and wait for the color that determines the formation of the amino acid.\r\n
▪	On the graph, observe the data about temperature vs. Time, and on the parameter indicators observe the volume, temperature and revolutions per minute.\r\n
▪	If the color test (Red) is not satisfactory, you must start over.\r\n
▪	The challenge ends once you obtain a satisfactory color in the test chamber. Note that each attempt will be registered in the lab evaluation report.\r\n
▪	At the end of the lab, make sure you download the activity report and submit it to your teacher as learning evidence.",


  "misionJuegoPracticaLibre" : " Welcome to the chemistry virtual lab for the study the synthesis of amino acids. On the table, you will find the equipment required for the synthesis of amino acids that includes an area with reagents and a solvent for synthesis of amino acids, a reagents conditioning chamber to test or verify the amino quality. You will need to take dosages of the reagents (solution A, B, C and D), condition the raw materials, process and test the amino acid.  Note that at the end of the situation, the result check is done with a colorimetric test." ,
   	 	
 "Instructions": "To find the data and calculations of the reactor associated with the synthesis of amino acids, which allow it to overcome the challenge, click the "Equations" button.\r\n
		
 Click the 'Calculator' button to perform the mathematical operations required to solve the equations to complete the challenge.  ",



 		"title" : "STATION 3: AMINO ACID SYNTHESIS REACTOR",
   	 	"situation" : "Synthesis of amino acid for preparation of product",
   	 	"unit" : "Amino acids and proteins",
   	 	"textMessageApproved" :  "Successful exercise, the data you have provided is correct.",
   	 	"textMessageNotApproved" :  "Exercise failed. The data you have provided is incorrect.",
		"GraphName" :  "Synthesis temperature vs. Time",
		"PdfName": "report_Synthesis_amino_acid"





    
    },


    "fermentador" : {
		"comoJugar":"To successfully complete this lab, complete the following procedures.\r\n\r\n
1. Identify the lab materials and functions.\r\n
\t\t\▪ Locate the bioreactor with actuators and automatic sensors.\r\n
\t\t\▪ Locate the control switches to adjust the following parameters:\r\n
\t\t\t\t\ ‣‣ Sugar solution: click to switch to add solution in liters (L).\r\n
\t\t\t\t\ ‣‣ Yeast solution: click to add solution in liters (L).\r\n
\t\t\t\t\ ‣‣ Acid solution: click to add solution in liters(L).\r\n
\t\t\t\t\ ‣‣ Base solution: click to add solution in liters (L).\r\n
\t\t\t\t\ ‣‣ Temperature: click the "+/-" button to regulate the temperature (ºC). \r\n
\t\t\t\t\ ‣‣ Agitator: click to turn on then adjust using the revolutions per
\r\n\t\t\t\t\t\minute (RPM) switch (+/-)\r\n
\t\t\t\t\ ‣‣ Relieve valve: click to control the pressure.\r\n
\t\t\▪ Locate the parameter table and indicator table to monitor the \r\n\t\t\ various processes.\r\n
\t\t\▪ The mission (challenge) is set for 4 minutes and will begin at the \r\n\t\t\ first release
\r\n\t\t\ of a solution into the mixing chamber.\r\n
\t\t\▪ The lab may be reset by clicking on the red "Reset" button. This will \r\n\t\t\ register an attempt.\r\n
\t\t\▪ A "Screen" button is also available to view an alcohol % and time graph,\r\n\t\t\ the chemical equations for the process, and the final data.\r\n\r\n

2. Prepare the mixing chamber for the fermentation process. \r\n
\t\Add sugar and yeast to the mixing chamber.  These may be added \r\n\t\ simultaneously. \r\n
\t\t\▪ Add 7 liters of sugar solution.\r\n
\t\t\▪ Add 30% yeast medium to the total solution of 10 liters.\r\n
\t\Facilitate the growth environment. \r\n
\t\t\▪ Turn on the agitator and set it to 100 rpms.\r\n
\t\t\▪ Set the temperature to 25º C.\r\n
\t\t\▪ Use the acid and base controls to set the pH between 4 and 5.\r\n\r\n

3. Monitor the fermentation process. \r\n
\t\t\▪ The goal is to produce a solution that is 45% percent alcohol within
\r\n\t\t\ the 4-minute time allotment. \r\n
\t\t\▪ Monitor the process using the parameter and indicator tables.\r\n\r\n

Completion of lab, answer the evaluation questions, and generate the lab report. \r\n
\t\t\▪ At the end of 4 minutes, if the goal has been met a success message \r\n\t\t\ will appear. Click "Continue".\r\n
\t\t\▪ If the experiment was not successful, a message will appear to retry.
\r\n\t\t\ Click "Retry". An attempt will be registered.\r\n\r\n

Optional: FREE PRACTICE \r\n
This lab has a free practice mode to allow for opening, ending, discovery and experimentation. After navigating to the lab station, choose the free practice, using the arrow button on the title screen.\r\n",
		
		
		
      "misionJuego" : "Welcome to the chemistry virtual lab for the study of alcoholic fermentation. There is a description below of the challenge that you will need to complete.\r\n

On the table, you will find equipment for the fermentation of alcohol. This equipment includes a feeding system for the solutions that will be fermented, the culture medium (yeast) for fermenting the solution, a container where the fermentation will take place, and the necessary sensors and actuators for the system to function properly. You will need to take dosages of the solutions (sugar solution and culture medium), and configure the equipment to ferment a solution with a final percentage of alcohol.",


      "Instructions": " Click the equations associated with distillation to find the data and calculations that will allow you to complete the challenge.\r\n
	  
	    Click the ‘Calculator’ button to perform the mathematical operations required for solving the equations and completing the challenge.",
	  
	  
   		"preguntaOrientadora1" : "Will the bacteria ferment the solution if we apply a temperature of 35 degrees to the culture medium? Please explain.",
   	 	"preguntaOrientadora2" : "Does fermentation provide the main product ethyl alcohol, additional carbon dioxide gas and also other by-products in very small quantities?. Please explain. ",
   	 	"preguntaOrientadora3" : " If the fermentation process is not adequately controlled, will the ethanoic acid be produced? Please explain. ",
   	 	"preguntaOrientadora4" : " What type of fermentation can be carried out with this type of fermenter?",
   	 	"preguntaOrientadora5" : " According to the fermentation equation, how much alcohol and how much carbon dioxide are produced in a fermentation process with 100g of a sugar solution?",
		
		
   	 	"preguntaEvaluacion1" : " The culture mediums used for fermentation are microorganisms known as yeasts.:true",
   	 	"preguntaEvaluacion2" : " The temperature required for an alcoholic fermentation with yeast is 45 degree Celsius?:false",
   	 	"preguntaEvaluacion3" : " In alcoholic fermentation carbon dioxide is generated as a byproduct, it can be stored for industrialization purposes?:true",
   	 	"preguntaEvaluacion4" : " If we find fermentation solution with a pH of nine (9), we can lower it by adding an acid solution.:true",
   	 	"preguntaEvaluacion5" : " The percentage of alcohol expected in a fermented solution is 85%.:false",



   	 	"comoJugarPracticaLibre":"▪	To perform the calculations use the data provided in the virtual lab including the sugar solution volume, the medium volume, speed, pH and temperature.\r\n
▪	Click the 'See reactions' button to observe the equations associated with alcoholic fermentation.\r\n
▪	To conduct the  fermentation follow these steps:\r\n
‣‣	Verify that the medium is ready for use.\r\n
‣‣	Adding to the fermenter the amount of sugar required to complete a 100% mixture.\r\n
‣‣	Add the microorganisms culture medium (30% of the total solution).\r\n
‣‣	Start the engine (100 rpm).\r\n
‣‣	Set the temperature (25 degrees Celsius) using the heat resistance.\r\n
‣‣	Set the pH (4-5) using the dosage of the  acidic medium as a basic medium.\r\n
‣‣	Wait until the activity time ends to see the results (4 minutes).\r\n
‣‣	Check the percentage of alcohol in the fermentation.\r\n
▪	On the screen, observe the data about temperature, pH, shaker speed, percentage of alcohol and the graphics of the available.\r\n
▪	At the end of the lab make sure you download the activity report and submit it to your teacher as learning evidence.",
 
	  
	  "misionJuegoPracticaLibre":"Welcome to the chemistry virtual lab for the study of alcoholic fermentation. There is a description below of the challenge that you will need to complete in order to successfully complete this lab.\r\n

On the table, you will find equipment for the fermentation of alcohols that includes a feeding system for the solutions that will be fermented, the culture medium (yeast) to ferment the solution, a main container where the fermentation will take place, and the necessary sensors and actuators for the system to function properly. You will need to take dosages of the solutions (sugar solution and culture medium), configure the equipment to ferment a solution with a final percentage of alcohol. ",

	   "Instructions": "Click the equations associated with fermentation to find the data and calculations that will allow it to complete the challenge .
	 Click the 'Calculator' button to perform the mathematical operations required to solve the equations to complete the challenge. ",



	    "title" : "STATION 1: FERMENTER",
   	 	"situation" : "Preparation of a fermented mixture in  45% alcohol",
   	 	"unit" : "Alcohols, aldehydes and ketones.",
   	 	"textMessageApproved" :  "Successful exercise. The data you have provided is correct.",
   	 	"textMessageNotApproved" :  "Exercise failed. The data you have provided is incorrect.",
		"GraphName" :  "Percent of alcohol vs. Time",
		"PdfName": "report_Preparation_fermented_mixture"
	
	  
        },




     "destilador" : {
		"comoJugar":      "To successfully complete this lab, complete the following procedures.\r\n\r\n
1. Identify the lab materials and functions.\r\n
\t\t\▪ Locate the distilling apparatus with actuators and automatic sensors.\r\n
\t\t\▪ Locate the control switches to adjust the following parameters:\r\n
\t\t\t\t\ ‣‣ Fermented solution: click to switch to add solution in liters (L).\r\n
\t\t\t\t\ ‣‣ Acid solution: click to add solution in liters(L).\r\n
\t\t\t\t\ ‣‣ Base solution: click to add solution in liters (L).\r\n
\t\t\t\t\ ‣‣ Temperature:  click "Set temperature", then click the "+/-" button to
\r\n\t\t\t\t\t\ regulate the temperature (ºC).\r\n
\t\t\t\t\ ‣‣ Water condenser: click to turn on.\r\n
\t\t\t\t\ ‣‣ Relieve valve: click to control the pressure.\r\n
\t\t\▪ Locate the parameter table and indicator table to monitor the
\r\n\t\t\ various processes.\r\n
\t\t\▪ The mission (challenge) is set for 6 minutes and will begin at the
\r\n\t\t\ first release of a solution into the distiller tank.\r\n
\t\t\▪ The lab may be reset by clicking on the red "Reset" button. This will
\r\n\t\t\ register an attempt.\r\n
\t\t\▪ A "Screen" button is also available to view an alcohol % and time graph,
\r\n\t\t\ the chemical equations for the process, and the final data.\r\n\r\n

2. Prepare the distillation process.\r\n
\t\t\▪ Add 3 liters of a fermented solution.\r\n
\t\t\▪ Set the temperature to 79º C.\r\n
\t\t\▪ Use the acid and base controls to set the pH to 6.\r\n
\t\t\▪ Click the water condenser control to allow water to flow through
\r\n\t\t\ the condensing coil.\r\n\r\n

3. Monitor the distillation process. \r\n
\t\t\▪ The goal is to produce a solution that is 95% percent alcohol within
\r\n\t\t\ the 6-minute time allotment.\r\n
\t\t\▪ Monitor the process using the parameter and indicator tables.\r\n\r\n

Complete the lab, answer the evaluation questions, and submit the lab report. \r\n
\t\t\▪ At the end of 6 minutes, if the goal has been met a success message 
\r\n\t\t\will appear. Click "Continue".\r\n
\t\t\▪ If the experiment was not successful, a message will appear to retry.
\r\n\t\t\ Click "Retry". An attempt will be registered.\r\n\r\n",

"misionJuego" : "Welcome to the virtual chemistry lab for studying the synthesis of amino acids. For your challenge, you will find elements and equipment on the lab table for the synthesis of amino acids. This includes an area with reagents and a solvent for the synthesis of amino acids, a reagents conditioning chamber, and the process chamber. There is also a test chamber, in which you can test or verify the quality of the amino acids.  You must prepare fifty (50) milliliters of an amino acid (glycine) with 60% of A and 40% of C. Note that at the end of the challenge, the result will be checked using a calorimetric test.",
		
      "misionJuego" : " Welcome to the chemistry virtual lab for the study of the distillation of alcohol. There is a description below of the challenge to complete in this lab.\r\n

On the table, you will find the equipment for the distillation of alcohols. This includes a system for the infusion of fermented solutions, a depository for the solution that will be be distilled, a column with a reflux system, a condenser and an alcohol collection container. You need to measure out the correct dosages required for the distillation (suitable for distilling fermented solution) and configure the computer to produce an alcohol solution with any percentage that lies within the indicated acceptable range.
 ",
 
 
       "Instructions": "Click the ‘Equations’ button to find the data and calculations that will allow you to complete the challenge. Click the ‘Calculator’ button to perform the necessary mathematical operations.  ",
	  
	  
   	 	"preguntaOrientadora1" : " Does the distillation process make use of the boiling points of liquids? What is a boiling point and how is it determined?",
   	 	"preguntaOrientadora2" : " What are the plates in a distillation tower? Please explain.  ",
   	 	"preguntaOrientadora3" : " Why can’t a distillation column produce 100% pure alcohol? What other process is required to do so?",
   	 	"preguntaOrientadora4" : " What is an isotropic mixture? ",
   	 	"preguntaOrientadora5" : " What is the molecular formula of ethyl alcohol and what are its most important characteristics? ",
		
		
   	 	"preguntaEvaluacion1" : " The culture medium for the distillation of alcohol consists of microorganisms known as yeast.:false",
   	 	"preguntaEvaluacion2" : " The temperature required for alcoholic distillation os 95 degrees Celsius.:false",
   	 	"preguntaEvaluacion3" : " The reflux system in the distillation process generates a steam rich in ethyl alcohol at the top of the distillation column.:true",
   	 	"preguntaEvaluacion4" : " To increase the pH of a solution pf pH 2, we need to add a basic solution.:true",
   	 	"preguntaEvaluacion5" : " The expected level of alcohol for the distilled solution is 95%.:true",




		"comoJugarPracticaLibre":"▪	To perform calculations use the data provided in the virtual lab including the volumes of the solution to distill, pH, and temperature.\r\n
▪	Click the 'Screen' button to see the equations associated with fermentation of alcohols.\r\n
▪	To distill alcohol in the distillation tower follow these steps:\r\n
‣‣	Verify that the fermented solution is ready for use. \r\n
‣‣	Add the solution that will be distilled to the distiller tank (round ball with a capacity of up to five (5) liters)\r\n
‣‣	Verify the water level in the condenser of the distillation tower. \r\n
‣‣	Check the pH (6) and temperature of the solution that will be distilled.\r\n
‣‣	Set the temperature to 79 Celsius degrees.\r\n
‣‣	Wait until the activity time ends to see the results (6 minutes).\r\n
‣‣	Measure the volume of the distilled alcohol.\r\n
▪	On the graph, observe the data about temperature vs. Time, and on the parameter indicators observe the temperature, pH, percentage and volume of alcohol for the distilled solution.\r\n
▪	At the end of the lab, make sure you download the activity report and submit it to your teacher as learning evidence.",
		
      "misionJuegoPracticaLibre" : " Welcome to the chemistry virtual lab for the study of the distillation of alcohol. There is a description below of the challenge to complete in this lab.\r\n

On the table, you will find the equipment for the distillation of alcohols including a system for the infusion of fermented solutions, a deposit for the solution to be distilled, a column with a reflux system, a condenser and an alcohol collection container. You need to make the dosages required for the distillation (suitable for distilling fermented solution) and configure the computer to produce an alcohol solution with any percentage within the range of the percentages displayed by the equipment. ",
      
  "Instructions": "Click the ‘Equations’ button to find the data and calculations that will allow you to complete the challenge . \r\n
  
   Click the ‘Calculator’ button to perform the necessary mathematical operations.  ",


        "title" : "STATION 2: DISTILLER",
        "situation" : "Distilling a solution of 95% alcohol (ethyl alcohol)",
        "unit" : "Alcohols, aldehydes and ketones.",
        "textMessageApproved" :  "Successful exercise! The data you have provided is correct.",
        "textMessageNotApproved" :  "Exercise failed. The data you have provided is incorrect.",
        "GraphName" :  "Percentage of alcohol vs. time",
        "PdfName": "report_Distilling_solution_alcohol"
		

    
    }



   


}
