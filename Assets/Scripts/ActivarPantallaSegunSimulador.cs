﻿using UnityEngine;

namespace NSUtilities
{
    public class ActivarPantallaSegunSimulador : MonoBehaviour
    {
        [SerializeField]
        private Simulador refSimulador;

        [SerializeField]
        private SimuladorFermentacion refSimuladorFermentacion;

        [SerializeField]
        private SimuladorDestilacion refSimuladorDestilacion;

        [SerializeField]
        private SimuladorAminoAcido refSimuladorAminoacido;

        public void OnButtonPantalla()
        {
             if (refSimulador.simuladorActivo.gameObject == refSimuladorAminoacido.gameObject)
                refSimuladorAminoacido.MostrarPantalla();            
        }
    }
}