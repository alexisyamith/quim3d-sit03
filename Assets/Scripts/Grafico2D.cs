﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Clase que dibuja las graficas
/// </summary>
public class Grafico2D : MonoBehaviour
{
    public Color32 colorCurva;
    public Color32 colorEjes;
    public Color32 colorEjes2;
    public GameObject panelGrafico;
    public Vector2 tamanoEscenario = new Vector2(10, 10);
    public Texture2D textureGraficoAminoacido;
    public Texture2D textureGraficoDestilacion;
    public Texture2D textureGraficoFermentacion;

    public Text uiLabelMaxXAminoacidos;
    public Text uiLabelMaxXDestilacion;
    public Text uiLabelMaxXFermentacion;

    public Text[] uiLabelsSubEjes;
    public RawImage uiTextureGraficoAminoacido;
    public RawImage uiTextureGraficoDestilacion;
    public RawImage uiTextureGraficoFermentacion;

    private void ActualizarRenderTexture(ref Texture2D textureGrafico, RawImage uiTexture)
    { 
        if (uiTexture != null)
        {
            if (textureGrafico == null || textureGrafico.width != uiTexture.mainTexture.width || textureGrafico.height != uiTexture.texture.height)
            {
                Destroy(textureGrafico);
                textureGrafico = new Texture2D(uiTexture.texture.width, uiTexture.texture.height, TextureFormat.ARGB32, false);
                textureGrafico.anisoLevel = 0;
                textureGrafico.filterMode = FilterMode.Point;
                textureGrafico.wrapMode = TextureWrapMode.Clamp;
                textureGrafico.Apply();
                uiTexture.texture = textureGrafico;
            }
        }
    }

    public void DibujarFermentacion(ref float[] segundos, ref float[] temperaturas, ref float[] porcentajeAlcohol)
    {
        // Actualizar la textura, en caso de que sea null o haya cambiado el tamano del UI
        ActualizarRenderTexture(ref textureGraficoFermentacion, uiTextureGraficoFermentacion);
        Texture2D textureGrafico = textureGraficoFermentacion;

        // Crear el arreglo de pixeles
        Color32[] pixels;
        InicializarArregloPixeles(out pixels, textureGrafico);

        // Dibujar los ejes
        int posEjeY = Mathf.RoundToInt(textureGrafico.width * 0.1f);
        int posEjeX = Mathf.RoundToInt(textureGrafico.height * 0.1f);
        DibujarEjes(textureGrafico, pixels, posEjeX, posEjeY);

        // Si solo queremos dibujar los ejes
        if (segundos.Length <= 2)
        {
            textureGrafico.SetPixels32(pixels);
            textureGrafico.Apply();
            return;
        }

        // Dibujar el tiro parabolico
        DibujarGraficoFermentacion(ref segundos, ref temperaturas, ref porcentajeAlcohol, textureGrafico, ref pixels, posEjeY,
            posEjeX);
    }

    public void DibujarDestilacion(ref float[] segundos, ref float[] temperaturas, ref float[] porcentajeAlcohol)
    {
        // Actualizar la textura, en caso de que sea null o haya cambiado el tamano del UI
        ActualizarRenderTexture(ref textureGraficoDestilacion, uiTextureGraficoDestilacion);
        Texture2D textureGrafico = textureGraficoDestilacion;

        // Crear el arreglo de pixeles
        Color32[] pixels;
        InicializarArregloPixeles(out pixels, textureGrafico);

        // Dibujar los ejes
        int posEjeY = Mathf.RoundToInt(textureGrafico.width * 0.1f);
        int posEjeX = Mathf.RoundToInt(textureGrafico.height * 0.1f);
        DibujarEjes(textureGrafico, pixels, posEjeX, posEjeY);

        // Si solo queremos dibujar los ejes
        if (segundos.Length <= 2)
        {
            textureGrafico.SetPixels32(pixels);
            textureGrafico.Apply();
            return;
        }
        // Dibujar el tiro parabolico
        DibujarGraficoDestilacion(ref segundos, ref temperaturas, ref porcentajeAlcohol, textureGrafico, ref pixels, posEjeY,
            posEjeX);
    }

    /// <summary>
    /// Dibujar grafica de los amino acidos
    /// </summary>
    public void DibujarAminoacido(ref float[] segundos, ref float[] temperaturas)
    {
        // Actualizar la textura, en caso de que sea null o haya cambiado el tamano del UI
        ActualizarRenderTexture(ref textureGraficoAminoacido, uiTextureGraficoAminoacido);
        Texture2D textureGrafico = textureGraficoAminoacido;

        // Crear el arreglo de pixeles
        Color32[] pixels;
        InicializarArregloPixeles(out pixels, textureGrafico);

        // Dibujar los ejes
        int posEjeY = Mathf.RoundToInt(textureGrafico.width * 0.1f);
        int posEjeX = Mathf.RoundToInt(textureGrafico.height * 0.1f);
        DibujarEjes(textureGrafico, pixels, posEjeX, posEjeY);

        if (segundos.Length <= 2)
        {
            textureGrafico.SetPixels32(pixels);
            textureGrafico.Apply();
            return;
        }

        // Dibujar el tiro parabolico
        DibujarGraficoAminoacido(ref segundos, ref temperaturas, textureGrafico, ref pixels, posEjeY, posEjeX);
    }

    private void DibujarGraficoFermentacion(ref float[] segundos, ref float[] temperaturas, ref float[] porcentajeAlcohol, Texture2D textureGrafico, ref Color32[] pixels, int posEjeY, int posEjeX)
    {
        // El eje Y tiene el porcentaje de alcohol
        float maxY = 100;
        // El eje X tiene el tiempo
        float tiempoTotal = segundos[segundos.Length - 1];

        // segundos
        float tamCurvaPixelesX = Mathf.RoundToInt(textureGrafico.width * 0.8f);
        float inicioCurvaPixelesX = 0;
        float deltaTiempo = tiempoTotal / tamCurvaPixelesX;
        int tamCurvaPixelesY = Mathf.RoundToInt(textureGrafico.height * 0.8f);
        float escalaCurvaY = maxY / tamCurvaPixelesY;
        float invEscalaCurvaY = 1 / escalaCurvaY;

        DibujarCurva(inicioCurvaPixelesX, tamCurvaPixelesX, deltaTiempo, invEscalaCurvaY, ref segundos, ref porcentajeAlcohol, ref pixels, posEjeY, posEjeX, textureGrafico);

        textureGrafico.SetPixels32(pixels);
        textureGrafico.Apply();

        // Actualizar los labels
        uiLabelMaxXFermentacion.text = tiempoTotal.ToString("n1") + "s";
    }

    private void DibujarGraficoDestilacion(ref float[] segundos, ref float[] temperaturas, ref float[] porcentajeAlcohol, Texture2D textureGrafico, ref Color32[] pixels, int posEjeY, int posEjeX)
    {
        // El eje Y tiene el porcentaje de alcohol
        float maxY = 100;
        // El eje X tiene el tiempo
        float tiempoTotal = segundos[segundos.Length - 1];

        // segundos
        float tamCurvaPixelesX = Mathf.RoundToInt(textureGrafico.width * 0.8f);
        float inicioCurvaPixelesX = 0;
        float deltaTiempo = tiempoTotal / tamCurvaPixelesX;
        int tamCurvaPixelesY = Mathf.RoundToInt(textureGrafico.height * 0.8f);
        float escalaCurvaY = maxY / tamCurvaPixelesY;
        float invEscalaCurvaY = 1 / escalaCurvaY;

        DibujarCurva(inicioCurvaPixelesX, tamCurvaPixelesX, deltaTiempo, invEscalaCurvaY, ref segundos, ref porcentajeAlcohol, ref pixels, posEjeY, posEjeX, textureGrafico);     

        textureGrafico.SetPixels32(pixels);
        textureGrafico.Apply();

        // Actualizar los labels
        uiLabelMaxXDestilacion.text = tiempoTotal.ToString("n1") + "s";
    }

    private void DibujarGraficoAminoacido(ref float[] segundos, ref float[] temperaturas, Texture2D textureGrafico, ref Color32[] pixels, int posEjeY, int posEjeX)
    {
        // El eje Y tiene la amplitud, es decir, el angulo
        float maxY = 100;
        // El eje X tiene el tiempo
        float tiempoTotal = segundos[segundos.Length - 1];

        // segundos
        float tamCurvaPixelesX = Mathf.RoundToInt(textureGrafico.width * 0.8f);
        float inicioCurvaPixelesX = 0;
        float deltaTiempo = tiempoTotal / tamCurvaPixelesX;
        int tamCurvaPixelesY = Mathf.RoundToInt(textureGrafico.height * 0.8f);
        float escalaCurvaY = maxY / tamCurvaPixelesY;
        float invEscalaCurvaY = 1 / escalaCurvaY;

        DibujarCurva(inicioCurvaPixelesX, tamCurvaPixelesX, deltaTiempo, invEscalaCurvaY, ref segundos, ref temperaturas, ref pixels, posEjeY, posEjeX, textureGrafico);
        
        textureGrafico.SetPixels32(pixels);
        textureGrafico.Apply();

        // Actualizar los labels
        uiLabelMaxXAminoacidos.text = tiempoTotal.ToString("n1") + "s";
    }

    private void DibujarCurva(float inicioCurvaPixelesX, float tamCurvaPixelesX, float deltaTiempo, float invEscalaCurvaY, ref float[] segundos, ref float[] datos, ref Color32[] pixels, int posEjeY, int posEjeX, Texture2D textureGrafico)
    {
        float posPixelX;
        int posPixelY, posPixelYAnterior = 0;
        int indexDatos = 0;
        float t = 0;
        float deltaX = 1;

        for (posPixelX = inicioCurvaPixelesX; posPixelX < tamCurvaPixelesX; posPixelX += deltaX)
        {
            t = posPixelX * deltaTiempo;
            while (t > segundos[indexDatos])
            {
                indexDatos++;
                if (indexDatos == segundos.Length)
                    break;
            }

            if (indexDatos == segundos.Length)
                break;

            posPixelY = Mathf.RoundToInt(datos[indexDatos] * invEscalaCurvaY);
            if (posPixelY - posPixelYAnterior == 0)
            {
                if (posEjeX + posPixelY < textureGrafico.height)
                    pixels[(posEjeX + posPixelY) * textureGrafico.width + (int)posPixelX + posEjeY] = colorCurva;
            }
            else
            {
                int delta = (int)Mathf.Sign(posPixelY - posPixelYAnterior);
                int start, end;
                if (delta > 0)
                {
                    start = posPixelYAnterior;
                    end = posPixelY;
                }
                else
                {
                    start = posPixelY;
                    end = posPixelYAnterior;
                }
                for (int y = start; y < end; y++)
                {
                    if (posEjeX + y < textureGrafico.height)
                        pixels[(posEjeX + y) * textureGrafico.width + (int)posPixelX + posEjeY] = colorCurva;
                }
                posPixelYAnterior = posPixelY;
            }
        }
    }

    private void DibujarEjes(Texture2D textureGrafico, Color32[] pixels, int posEjeX, int posEjeY)
    {
        const int numEjesX = 20;
        const int numEjesY = 10;
        float deltaEspacioX = textureGrafico.width / (float)numEjesX;
        float deltaEspacioY = textureGrafico.height / (float)numEjesY;
        int filaInicioY = (int)(deltaEspacioY) + 1;
        int filaFinY = textureGrafico.height - filaInicioY;
        int colInicioX = (int)(deltaEspacioX * 2) + 1;
        int colFinX = textureGrafico.width - colInicioX;
        // Sub ejes X y Y
        for (int y = filaInicioY; y < filaFinY; y++)
        {
            for (int x = 2; x < numEjesX - 1; x++)
            {
                pixels[y * textureGrafico.width + Mathf.RoundToInt(deltaEspacioX * x)] = colorEjes2;
            }
        }
        for (int x = colInicioX; x < colFinX; x++)
        {
            pixels[posEjeX * textureGrafico.width + x] = colorEjes;
            for (int y = 1; y < numEjesY; y++)
            {
                pixels[Mathf.RoundToInt(deltaEspacioY * y) * textureGrafico.width + x] = colorEjes2;
            }
        }
        // Eje X y Y
        for (int x = colInicioX; x < colFinX; x++)
        {
            pixels[posEjeX * textureGrafico.width + x] = colorEjes;
        }
        for (int y = filaInicioY; y < filaFinY; y++)
        {
            pixels[y * textureGrafico.width + posEjeY] = colorEjes;
        }

        textureGrafico.SetPixels32(pixels);
        textureGrafico.Apply();
    }

    private void InicializarArregloPixeles(out Color32[] pixels, Texture2D textureGrafico)
    {
        // Crear el arreglo de pixeles
        pixels = new Color32[textureGrafico.width * textureGrafico.height];
        for (int y = 0; y < textureGrafico.height; y++)
        {
            for (int x = 0; x < textureGrafico.width; x++)
            {
                pixels[y * textureGrafico.width + x] = new Color32(0, 0, 0, 0);
            }
        }
    }
}