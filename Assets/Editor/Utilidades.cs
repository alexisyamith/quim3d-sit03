﻿using UnityEngine;
using UnityEditor;

public class Utilidades
{
    [MenuItem("GenMedia/AULA-Sel/ON")]
    static void AULASel_ON()
    {
        GameObject.FindObjectOfType<Simulador>().ModoAula = true;
    }

    [MenuItem("GenMedia/AULA-Sel/OFF")]
    static void AULASel_OFF()
    {
        GameObject.FindObjectOfType<Simulador>().ModoAula = false;
    }

    [MenuItem("GenMedia/IdiomSel/ON_ENG-SPA")]
    static void LangSel_ON_EngSpa()
    {
        GameObject.FindObjectOfType<Pantalla>().SelLangOption = true;
        GameObject.FindObjectOfType<Pantalla>().pairLanguages = "ENG-SPA";
    }

    [MenuItem("GenMedia/IdiomSel/ON_ENG-POR")]
    static void LangSel_ON_EngPor()
    {
        GameObject.FindObjectOfType<Pantalla>().SelLangOption = true;
        GameObject.FindObjectOfType<Pantalla>().pairLanguages = "ENG-POR";
    }

    [MenuItem("GenMedia/IdiomSel/OFF")]
    static void LangSel_OFF()
    {
        GameObject.FindObjectOfType<Pantalla>().SelLangOption = false;
    }

    [MenuItem("GenMedia/Export_iOS/Spa")]
    static void ExpEng()
    {
        PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "CloudLabs Química 3D";//Nombre de la aplicación
        PlayerSettings.applicationIdentifier = "com.cloudlabs.quimica3d.developer2";
    }

    [MenuItem("GenMedia/Export_iOS/Eng-Multi")]
    static void ExpSpa()
    {
        PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "Chemistry CloudLabs 3D";//Nombre de la aplicación
        PlayerSettings.applicationIdentifier = "com.cloudlabs.chemistry3d.developer2";
    }

    [MenuItem("GenMedia/Bundle/Eng")]
    static void BundleEng()
    {
        PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "Chemistry CloudLabs";//Nombre de la aplicación
        PlayerSettings.applicationIdentifier = "com.genmedia.quimicaesp";
    }

    [MenuItem("GenMedia/Bundle/Spa")]
    static void BundleSpa()
    {
        PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "Química CloudLabs";//Nombre de la aplicación
        PlayerSettings.applicationIdentifier = "com.genmedia.quimicaesp";
    }

    [MenuItem("GenMedia/Seguridad/Seguridad_ON")]
    static void ActivarSeguridad()
    {
        //GameObject.FindObjectOfType<Simulador> ().UsarLogo1 ();
        GameObject.FindObjectOfType<Simulador>().ActivarSeguridad();
        Debug.Log(GameObject.FindObjectOfType<Simulador>().SeguridadSwitch);
    }

    [MenuItem("GenMedia/Seguridad/Seguridad_OFF")]
    static void QuitarSeguridad()
    {
        GameObject.FindObjectOfType<Simulador>().QuitarSeguridad();
        Debug.Log(GameObject.FindObjectOfType<Simulador>().SeguridadSwitch);
    }

    [MenuItem("GenMedia/Escenario/DesactivarObjeto Selection _h")]
    static void DesactivarObjeto()
    {
        var go = Selection.activeGameObject;

        if (go.activeInHierarchy == true)
        {
            go.SetActive(false);
        }
        else
        {
            go.SetActive(true);
        }
    }

    [MenuItem("GenMedia/Idiomas/Cambiar a Ingles")]
    static void CambiarIngles()
    {
        PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "Chemistry CloudLabs";//Nombre de la aplicación
        PlayerSettings.applicationIdentifier = "com.genmedia.quimicaesp";

#if UNITY_IOS
		if(GameObject.FindObjectOfType<Simulador>().SeguridadSwitch)
        {
			PlayerSettings.applicationIdentifier = "com.genmedia.quimicaeng";
		}
#endif

        ControlIdiomas idioma = GameObject.FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Ingles;
        SimuladorGenerico[] simuladores = GameObject.FindObjectsOfType<SimuladorGenerico>();

        foreach (SimuladorGenerico sim in simuladores)
        {
            Debug.Log("cambiando idioma" + sim.name);
            sim.CargarTextos();
        }
    }

    [MenuItem("GenMedia/Idiomas/Cambiar a Español")]
    static void CambiarEspanol()
    {
        PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "Química CloudLabs";//Nombre de la aplicación
        PlayerSettings.applicationIdentifier = "com.genmedia.quimicaesp";

        ControlIdiomas idioma = GameObject.FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Espanol;        
        SimuladorGenerico[] simuladores = GameObject.FindObjectsOfType<SimuladorGenerico>();

        foreach (SimuladorGenerico sim in simuladores)
        {
            Debug.Log("cambiando idioma" + sim.name);
            sim.CargarTextos();
        }
    }

    [MenuItem("GenMedia/Idiomas/Cambiar a Portugues")]
    public static void CambiarPortugues()
    {
        PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "Química CloudLabs";//Nombre de la aplicación

        PlayerSettings.applicationIdentifier = "com.genmedia.quimicaesp";
        Debug.Log(PlayerSettings.applicationIdentifier);

        ControlIdiomas idioma = GameObject.FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Portugues;
        SimuladorGenerico[] simuladores = GameObject.FindObjectsOfType<SimuladorGenerico>();

        foreach (SimuladorGenerico sim in simuladores)
        {
            Debug.Log("cambiando idioma" + sim.name);
            sim.CargarTextos();
        }
    }

    [MenuItem("GenMedia/Idiomas/Cambiar a Turco")]
    static void CambiarTurco()
    {
        PlayerSettings.companyName = "CloudLabs";//cambio de Compañia si es necesario
        PlayerSettings.productName = "Kimya CloudLabs";//Nombre de la aplicación
        PlayerSettings.applicationIdentifier = "com.genmedia.quimicaesp";

#if UNITY_IOS
        if(GameObject.FindObjectOfType<Simulador>().SeguridadSwitch)
        {
            PlayerSettings.applicationIdentifier = "com.genmedia.quimicaeng";
        }
#endif

        ControlIdiomas idioma = GameObject.FindObjectOfType<ControlIdiomas>();
        idioma.idioma = ControlIdiomas.Idioma.Turco;
        SimuladorGenerico[] simuladores = GameObject.FindObjectsOfType<SimuladorGenerico>();

        foreach (SimuladorGenerico sim in simuladores)
        {
            Debug.Log("cambiando idioma" + sim.name);
            sim.CargarTextos();
        }
    }

    [MenuItem("GenMedia/Logo/Usar Logo N1")]
    static void UsarLogo1()
    {
        GameObject.FindObjectOfType<Simulador>().UsarLogo1();
    }

    [MenuItem("GenMedia/Logo/Usar Logo N2")]
    static void UsarLogo2()
    {
        GameObject.FindObjectOfType<Simulador>().UsarLogo2();
    }

    //Configuraciones para cloud build.
    public static void InglesLogo1()
    {
        UsarLogo1();
        CambiarIngles();
        PlayerSettings.applicationIdentifier = "com.genmedia.quimicaesp";
    }

    public static void InglesLogo2()
    {
        CambiarIngles();
        UsarLogo2();
        PlayerSettings.applicationIdentifier = "com.genmedia.quimicaesp";
    }

    public static void EspanolLogo1()
    {
        UsarLogo1();
        CambiarEspanol();
        PlayerSettings.applicationIdentifier = "com.genmedia.quimicaesp";
    }

    public static void EspanolLogo2()
    {
        UsarLogo2();
        CambiarEspanol();
        PlayerSettings.applicationIdentifier = "com.genmedia.quimicaesp";
    }
}
